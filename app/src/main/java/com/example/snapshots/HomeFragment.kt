package com.example.snapshots

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.snapshots.databinding.FragmentHomeBinding
import com.example.snapshots.databinding.ItemSnapshotBinding
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage


class HomeFragment : Fragment(), HomeAux {

    private lateinit var mBinding: FragmentHomeBinding

    private lateinit var mFirebaseAdapter: FirebaseRecyclerAdapter<Snapshot,SnapshotHolder>

    private lateinit var mLayoutManager:RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentHomeBinding.inflate(inflater,container,false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val query = FirebaseDatabase.getInstance().reference.child("snapshots")
        val options = FirebaseRecyclerOptions.Builder<Snapshot>().setQuery(query) {
            val snapshot = it.getValue(Snapshot::class.java)
            snapshot!!.id = it.key!!
            snapshot
        }.build()
        //options = FirebaseRecyclerOptions.Builder<Snapshot>() ->
        //.setQuery(query,Snapshot::class.java).build()

        mFirebaseAdapter = object :FirebaseRecyclerAdapter<Snapshot,SnapshotHolder>(options){
            private lateinit var mContext:Context

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SnapshotHolder {
                mContext = parent.context

                val viw = LayoutInflater.from(mContext).inflate(R.layout.item_snapshot, parent,false)

                return SnapshotHolder(viw)
            }

            override fun onBindViewHolder(holder: SnapshotHolder, position: Int, model: Snapshot) {
                val snapshot = getItem(position)

                with(holder){
                    setListener(snapshot)
                    binding.tvTitle.text = snapshot.title
                    binding.cbLike.text = snapshot.likeList.keys.size.toString()//cuantos like tiene
                    //la imagen.
                    //aqui podemos consultamos si hemos dado like a esa imagen
                    FirebaseAuth.getInstance().currentUser?.let {
                        binding.cbLike.isChecked = snapshot.likeList
                            .containsKey(it.uid)
                    }


                    Glide.with(mContext)
                        .load(snapshot.photoUrl)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        //.centerCrop()
                        .into(binding.imgPhoto)
                    binding.btnDelete.visibility = if (model.ownerUid == FirebaseAuth.getInstance().currentUser?.uid){
                        View.VISIBLE
                    }else{
                        View.INVISIBLE
                    }

                }
            }

            @SuppressLint("NotifyDataSetChanged")//error interno firebase 8.0.0
            override fun onDataChanged() {
                super.onDataChanged()
                mBinding.pbHome.visibility = View.GONE
                notifyDataSetChanged()
            }

            override fun onError(error: DatabaseError) {
                super.onError(error)
                Toast.makeText(mContext,error.message,Toast.LENGTH_LONG).show()
            }

        }

        mLayoutManager = LinearLayoutManager(context)

        mBinding.rvHome.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFirebaseAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }

    override fun goToTop() {
        mBinding.rvHome.smoothScrollToPosition(0)
    }

    private fun deleteSnapshot(snapshot: Snapshot){
       // snapshot.id = 1.toString()//ejemplo de delete snapshot con valores estaticos y
        ///tenemos que quitar estos valores una vez excluyamos el id de firebase en android
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle("¿Eliminar instantanea?")
                .setPositiveButton("Eliminar"){_,_ ->
                    val databaseReferences = FirebaseDatabase.getInstance().reference.child("snapshots")
                    val storageSnapshotRef = FirebaseStorage.getInstance().reference.child("snapshots")
                        .child(FirebaseAuth.getInstance().currentUser!!.uid)
                        .child(snapshot.id)
                    storageSnapshotRef.delete().addOnCompleteListener {result ->
                        if (result.isSuccessful){
                            databaseReferences.child(snapshot.id).removeValue()
                        }else{
                            Snackbar.make(mBinding.root,getString(R.string.home_delete_photo_error), Snackbar.LENGTH_LONG).show()
                        }
                    }


                }
                .setNegativeButton("Cancelar",null)
                .show()
        }
    }
    private fun setLike(snapshot: Snapshot, checked: Boolean) {
        val databaseReference = FirebaseDatabase.getInstance().reference.child("snapshots")
        if (checked){
            databaseReference.child(snapshot.id).child("likeList")
                .child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(checked)
        }else{
            databaseReference.child(snapshot.id).child("likeList")
                .child(FirebaseAuth.getInstance().currentUser!!.uid).setValue(null)
        }

    }

    inner class SnapshotHolder(view: View):RecyclerView.ViewHolder(view){
        val binding = ItemSnapshotBinding.bind(view)

        fun setListener(snapshot: Snapshot){
            binding.btnDelete.setOnClickListener { deleteSnapshot(snapshot) }
            binding.cbLike.setOnCheckedChangeListener { _, checked ->
                setLike(snapshot,checked)
            }
        }
    }
}