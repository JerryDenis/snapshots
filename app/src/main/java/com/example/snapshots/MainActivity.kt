package com.example.snapshots

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.snapshots.databinding.ActivityMainBinding
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.FirebaseAuthUIActivityResultContract
import com.firebase.ui.auth.data.model.FirebaseAuthUIAuthenticationResult
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var mbinding:ActivityMainBinding

    private lateinit var mActiveFragment: Fragment
    private var mFragmentManager: FragmentManager? = null

    private lateinit var mAuthListener: FirebaseAuth.AuthStateListener
    private var mFirebaseAuth:FirebaseAuth? = null
    val providers = arrayListOf(
        AuthUI.IdpConfig.EmailBuilder().build(),
        AuthUI.IdpConfig.GoogleBuilder().build()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mbinding.root)

        setupAuth()

    }
    private fun onSignInResult(result: FirebaseAuthUIAuthenticationResult) {
        val response = result.idpResponse
        if (result.resultCode == RESULT_OK) {
            // Successfully signed in
            val user = mFirebaseAuth?.currentUser
            Toast.makeText(this,"Bienvenido {$user}",Toast.LENGTH_LONG).show()
        } else {
            if (response == null){
                finish()
            }
            // Sign in failed. If response is null the user canceled the
            // sign-in flow using the back button. Otherwise check
            // response.getError().getErrorCode() and handle the error.
            // ...
        }
    }
    private val signInLauncher = registerForActivityResult(
        FirebaseAuthUIActivityResultContract()
    ) { res ->
        this.onSignInResult(res)
    }

    private fun setupAuth() {
        mFirebaseAuth = FirebaseAuth.getInstance()
        mAuthListener = FirebaseAuth.AuthStateListener {
            var user = it.currentUser
            if (user == null){
                val signInIntent = AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setIsSmartLockEnabled(false)// Para no mostrar cuentas logeadas
                    .setAvailableProviders(providers)
                    .build()
                signInLauncher.launch(signInIntent)
                mFragmentManager = null

            }else{
                user = mFirebaseAuth?.currentUser
                val fragmentProfile = mFragmentManager?.findFragmentByTag(ProfileFragment::class.java.toString())
                fragmentProfile?.let {
                    (it as FragmentAux).Refresh()
                }
                if (mFragmentManager == null){
                    mFragmentManager = supportFragmentManager
                    setupBottomNav(mFragmentManager!!)
                }

            }
        }
    }

    private fun setupBottomNav(fragmentManager:FragmentManager){
        //mFragmentManager = supportFragmentManager
        mFragmentManager?.let {
            for (fragment in it.fragments){
                it.beginTransaction().remove(fragment!!).commit()
            }
        }

        //declaramos todos los fragments
        val homeFragment = HomeFragment()
        val addFragment = AddFragment()
        val profileFragment = ProfileFragment()

        mActiveFragment = homeFragment


       with(mFragmentManager!!.beginTransaction()){
            add(R.id.hostFragment, profileFragment, ProfileFragment::
            class.java.name).hide(profileFragment)
            add(R.id.hostFragment, addFragment, AddFragment::
            class.java.name).hide(addFragment)
            add(R.id.hostFragment, homeFragment, HomeFragment::
            class.java.name).commit()
        }
        /*
        //para poder mostrar el homefragment en primer lugar debemos
        //realizarlo a la inversa el orden
        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment,profileFragment,ProfileFragment::class.java.name)
            .hide(profileFragment)
            .commit()

        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment,addFragment,AddFragment::class.java.name)
            .hide(addFragment)
            .commit()

        mFragmentManager.beginTransaction()
            .add(R.id.hostFragment,homeFragment,HomeFragment::class.java.name)
            //.hide(homeFragment)el home ya no vamos a ocultar porque queremos que se vea desde el inicio
            .commit()*/
        //aqui se declara el evento u oyente de cada bottom navigation para cada fragmento respectivo
        mbinding.bottomNav.setOnItemSelectedListener {
            when(it.itemId){
                R.id.action_home->{
                    bnFragment(homeFragment)
                   /* mFragmentManager.beginTransaction().hide(mActiveFragment).show(homeFragment).commit()
                    mActiveFragment = homeFragment*/
                    true
                }
                R.id.action_add->{
                    bnFragment(addFragment)
                    /*mFragmentManager.beginTransaction().hide(mActiveFragment).show(addFragment).commit()
                    mActiveFragment = addFragment*/
                    true
                }
                R.id.action_profile->{
                    bnFragment(profileFragment)
                    /*mFragmentManager.beginTransaction().hide(mActiveFragment).show(profileFragment).commit()
                    mActiveFragment = profileFragment*/
                    true
                }

                else -> false
            }
        }
        mbinding.bottomNav.setOnItemReselectedListener {
            when(it.itemId){
                R.id.action_home -> (homeFragment as FragmentAux).Refresh()
            }
        }

    }
    private fun bnFragment(frag: Fragment) {
        mFragmentManager?.beginTransaction()
            ?.hide(mActiveFragment)?.show(frag)?.commit()
        mActiveFragment = frag
    }
    override fun onResume() {
        super.onResume()
        mFirebaseAuth?.addAuthStateListener(mAuthListener)
    }

    override fun onPause() {
        super.onPause()
        mFirebaseAuth?.removeAuthStateListener(mAuthListener)

    }


}