package com.example.snapshots

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import com.example.snapshots.databinding.FragmentAddBinding
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference


class AddFragment : Fragment() {

    private lateinit var mbinding:FragmentAddBinding

    private val RC_GALLERY = 18
    private val PATH_SNAPSHOT = "snapshots"

    private var mPhotoSelectUri: Uri? = null
    private lateinit var mStorageReference: StorageReference
    private lateinit var mDataBaseReference: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mbinding = FragmentAddBinding.inflate(inflater,container,false)
        return mbinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mbinding.btPost.setOnClickListener { postSnapshot() }
        mbinding.btnImageSelect.setOnClickListener { openGallery() }
        mStorageReference = FirebaseStorage.getInstance().reference
        mDataBaseReference = FirebaseDatabase.getInstance().reference.child(PATH_SNAPSHOT)
    }

    private fun openGallery() {
        galleryResult.launch(Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
       /* val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryResult.launch(Intent(intent))*/

    }

    private fun postSnapshot() {
        mbinding.pbAdd.visibility = View.VISIBLE
        val key = mDataBaseReference.push().key!!
        val storageReference = mStorageReference.child(PATH_SNAPSHOT)
            .child(FirebaseAuth.getInstance().currentUser!!.uid).child(key)
        if (mPhotoSelectUri != null){
            storageReference.putFile(mPhotoSelectUri!!)
                .addOnProgressListener {
                    val progress = (100 * it.bytesTransferred/it.totalByteCount).toDouble()
                    mbinding.pbAdd.progress = progress.toInt()
                    mbinding.tvMessage.text = "$progress%"
                }
                .addOnCompleteListener {
                    mbinding.pbAdd.visibility = View.INVISIBLE
                }
                .addOnSuccessListener {
                    Snackbar.make(mbinding.root,"Instantánea publicada.",Snackbar.LENGTH_SHORT).show()
                    it.storage.downloadUrl.addOnSuccessListener {
                        saveSnapshot(FirebaseAuth.getInstance().currentUser!!.uid,key,it.toString(),mbinding.etTitle.text.toString())
                        with(mbinding){
                            tilTitle.visibility = View.GONE
                            tvMessage.text = getString(R.string.post_message_title)
                            imagePhoto.setImageURI(null)
                        }

                    }
                }
                .addOnFailureListener {
                    Snackbar.make(mbinding.root,"No se pudo subir intente mas tarde.",Snackbar.LENGTH_SHORT).show()
                }
        }
    }
    private fun saveSnapshot(ownerUid: String, key: String, url:String, title:String){
        val snapshot = Snapshot(ownerUid = ownerUid,title = title, photoUrl = url)
        mDataBaseReference.child(key).setValue(snapshot)

    }
    private val galleryResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if (it.resultCode == Activity.RESULT_OK){
            mPhotoSelectUri = it.data?.data

            with(mbinding){
                imagePhoto.setImageURI(mPhotoSelectUri)
                tilTitle.visibility = View.VISIBLE
                tvMessage.text = getString(R.string.post_message_valid_title)
            }
        }
    }

}